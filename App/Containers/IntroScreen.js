import React, { Component } from 'react'
import { ImageBackground, Text, Image } from 'react-native'
import { connect } from 'react-redux'
import AppIntroSlider from 'react-native-app-intro-slider'
import LaunchScreen from './LaunchScreen'
import { Images } from '../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/IntroScreenStyle'

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    image: Images.intro1,
    backgroundColor: '#59b2ab',
    backgroundImage: Images.introBackground
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Other cool stuff',
    image: Images.intro2,
    backgroundColor: '#febe29',
    backgroundImage: Images.introBackground
  },
  {
    key: 'somethun1',
    title: 'Title 3',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    image: Images.intro3,
    backgroundColor: '#22bcb5',
    backgroundImage: Images.introBackground
  }
]

class IntroScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showRealApp: false
    }
  }

  _renderItem = (item) => {
    return (
      <ImageBackground source={item.backgroundImage} style={[styles.mainContent, {backgroundColor: item.backgroundColor}]}>
        <Text style={styles.title}>{item.title}</Text>
        <Image resizeMode={'contain'} style={styles.image} source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </ImageBackground >
    )
  }
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true })
  }
  render () {
    if (this.state.showRealApp) {
      return <LaunchScreen />
    } else {
      return <AppIntroSlider renderItem={this._renderItem} slides={slides} onDone={this._onDone} />
    }
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IntroScreen)
