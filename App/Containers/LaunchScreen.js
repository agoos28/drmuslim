import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import firebase from 'react-native-firebase';
import BannerWaktuSholat from '../Components/BannerWaktuSholat'
import { Images } from '../Themes'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default class LaunchScreen extends Component {
  constructor (props) {
    super(props)
  }

  async componentDidMount () {
    const enabled = await firebase.messaging().hasPermission()
    if (enabled) {
      const notificationOpen = await firebase.notifications().getInitialNotification();
      if (notificationOpen) {
        const action = notificationOpen.action;
        console.log(action)
        const Notification = notificationOpen.notification;
        console.log(Notification)
      }
    } else {
      try {
        await firebase.messaging().requestPermission();
      } catch (error) {
        console.log(error)
      }
    }
  }

  componentWillUnmount() {
    this.notificationOpenedListener();
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.container}>
          <BannerWaktuSholat />
          <View style={styles.section} >
            <Image source={Images.ready} />
            <Text style={styles.sectionText}>
              This probably isn't what your app is going to look like. Unless your designer handed you this screen and, in that case, congrats! You're ready to ship. For everyone else, this is where you'll see a live preview of your fully functioning app using Ignite.
            </Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}
