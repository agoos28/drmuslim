import React, { Component } from 'react'
import moment from 'moment'
import { ImageBackground, Text } from 'react-native'
import { prayersCalc } from 'prayer-timetable-lib'
import hijriDate from '../Transforms/ConvertToHijri'
import styles from './Styles/BannerWaktuSholatStyle'
import { Images } from '../Themes'

export default class BannerWaktuSholat extends Component {
  constructor (props) {
    super(props)
    const date = new Date()
    this.state = {
      dateTime: {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear(),
        hours: date.getHours(),
        min: date.getMinutes(),
        sec: date.getSeconds()
      }
    }
  }
  updateTime () {
    const date = new Date()
    this.setState({
      dateTime: {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear(),
        hours: date.getHours(),
        min: date.getMinutes(),
        sec: date.getSeconds()
      }
    })
  }
  prayerList () {
    const settings = {
      'hijrioffset': '1',
      'join': '0',
      'jummuahtime': [13, 15],
      'taraweehtime': [23, 15],
      'jamaahmethods': ['beforenext', '', 'beforenext', 'afterthis', 'afterthis', 'fixed'],
      'jamaahoffsets': [[0, 30], [], [0, 15], [0, 15], [0, 15], [22, 25]],

    }
    const timetable = require('../Fixtures/prayerTimeTable')

    const { prayers, isAfterIsha } = prayersCalc(timetable, settings)

    const prayerList = isAfterIsha ? prayers.tomorrow : prayers.today
    let current
    prayerList.some((prayer) => {
      if (prayer.hasPassed === false) {
        console.log(prayer)
        current = prayer
        return true
      }
    })
    return current
  }
  componentDidMount () {
    this.interval = setInterval(() => {
      this.updateTime()
    }, 60000)
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }

  render () {
    const currentPray = this.prayerList()
    const names = ['Imsyak', 'Shubuh', 'Dzuhur', 'Ashr', 'Maghrib', 'Isya']
    const months = ['Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember']
    const hijr = hijriDate()
    const date = this.state.dateTime
    const sisaWaktu = currentPray.time.fromNow().replace('in ', '').replace('hours','jam').replace('minutes','menit')
    return (
      <ImageBackground source={Images.ashar} style={{width: styles.screenWidth, height: styles.screenWidth / 2, padding: 24}}>
        <Text style={{fontSize: 11, color: '#ffffff'}}>{date.day + ' ' + months[date.month - 1] + ' ' + date.year}</Text>
        <Text style={{fontSize: 11, color: '#ffffff'}}>{hijr.dayMonth + ' ' + hijr.monthText + ' ' + hijr.year}</Text>
        <Text style={{fontSize: 48, color: '#ffffff'}}>{names[currentPray.index]}</Text>
        <Text style={{fontSize: 24, lineHeight: 24, marginBottom: 26, color: '#ffffff'}}>{currentPray.time.format('HH:mm')}</Text>
        <Text style={{fontSize: 11, color: '#ffffff'}}>{sisaWaktu} menjelang {names[currentPray.index]}</Text>
      </ImageBackground>
    )
  }
}
