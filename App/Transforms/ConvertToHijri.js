const basecal = (date, adjust) => {
  let today = date
  if (adjust) {
    let adjustmili = 1000 * 60 * 60 * 24 * adjust
    let todaymili = today.getTime() + adjustmili
    today = new Date(todaymili)
  }
  let wd = date.getDay() + 1

  let day = today.getDate()
  let month = today.getMonth()
  let year = today.getFullYear()
  let m = month + 1
  let y = year
  if (m < 3) {
    y -= 1
    m += 12
  }

  let a = Math.floor(y / 100.0)
  let b = 2 - a + Math.floor(a / 4.0)
  if (y < 1583) b = 0
  if (y === 1582) {
    if (m > 10) b = -10
    if (m === 10) {
      b = 0
      if (day > 4) b = -10
    }
  }

  let jd = Math.floor(365.25 * (y + 4716)) + Math.floor(30.6001 * (m + 1)) + day + b - 1524

  b = 0
  if (jd > 2299160) {
    a = Math.floor((jd - 1867216.25) / 36524.25)
    b = 1 + a - Math.floor(a / 4.0)
  }
  let bb = jd + b + 1524
  let cc = Math.floor((bb - 122.1) / 365.25)
  let dd = Math.floor(365.25 * cc)
  let ee = Math.floor((bb - dd) / 30.6001)
  day = (bb - dd) - Math.floor(30.6001 * ee)
  month = ee - 1
  if (ee > 13) {
    cc += 1
    month = ee - 13
  }
  year = cc - 4716

  let iyear = 10631.0 / 30.0
  let epochastro = 1948084

  let shift1 = 8.01 / 60.0

  let z = jd - epochastro
  let cyc = Math.floor(z / 10631.0)
  z = z - 10631 * cyc
  let j = Math.floor((z - shift1) / iyear)
  let iy = 30 * cyc + j
  z = z - Math.floor(j * iyear + shift1)
  let im = Math.floor((z + 28.5001) / 29.5)
  if (im === 13) {
    im = 12
  }
  let id = z - Math.floor(29.5001 * im - 29)

  let myRes = new Array(8)

  myRes[0] = day
  myRes[1] = month - 1
  myRes[2] = year
  myRes[3] = jd - 1
  myRes[4] = wd - 1
  myRes[5] = id
  myRes[6] = im - 1
  myRes[7] = iy
  return myRes
}

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

export default (date, adjustment) => {
  if (!date) {
    date = new Date()
  }
  let wdNames = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu']
  let iMonthNames = ['Muharram', 'Safar', 'Rabi\'ul Awal', 'Rabi\'ul Akhir',
    'Jumadil Awal', 'Jumadil Akhir', 'Rajab',
    'Sya\'ban',
    'Ramadhan', 'Syawal', 'Dzulkaidan',
    'Dzulhijjah']
  let iDate = basecal(date, adjustment)
  return {
    day: wdNames[iDate[4]],
    dayMonth: pad(iDate[5], 2),
    month: iDate[6] + 1,
    monthText: iMonthNames[iDate[6]],
    year: iDate[7]
  }
}
